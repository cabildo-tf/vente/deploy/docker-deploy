ARG UBUNTU_VERSION=focal
ARG VPN_VERSION=v1.0

FROM ubuntu:${UBUNTU_VERSION} as builder

ENV DOCKER_COMPOSE_VERSION=1.27.4

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        ca-certificates \
        wget && \
    rm -rf /var/lib/apt/lists/* && \
    wget -O /usr/bin/docker-compose --progress=dot:giga \
         "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-Linux-x86_64" && \
    chmod +x /usr/bin/docker-compose

FROM registry.gitlab.com/cabildo-tf/vente/deploy/vpn:${VPN_VERSION}

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        openssh-client \
        gettext && \
    rm -rf /var/lib/apt/lists/*

COPY /rootfs /

COPY --from=builder /usr/bin/docker-compose /usr/bin/docker-compose

ENTRYPOINT ["/bin/bash", "-c"]