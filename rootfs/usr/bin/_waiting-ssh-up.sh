#!/bin/bash

RETRIES=30

echo -e "${INFO_COLOR}Waiting for SSH service is running${NULL_COLOR}"

until [ ${RETRIES} -eq 0 ]
do 
    if ssh -T -q ${SSH_PARAMS} "${SSH_REMOTE}" exit
    then
        echo -e "\\n${INFO_COLOR}SSH service is up${NULL_COLOR}"
        break
    fi

    echo -ne "."

    RETRIES=$((RETRIES-=1))
    sleep 1
done

if [ ${RETRIES} -eq 0 ]
then
    echo -e "\\n${FAIL_COLOR}SSH connection has failed!${NULL_COLOR}"
    exit 1
fi